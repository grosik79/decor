package pl.sda.barista.pl.sda.frother;

public class MilkFrotherException extends Exception {
    public MilkFrotherException(String message) {
        super(message);
    }
}
