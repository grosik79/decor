package pl.sda.barista.pl.sda.frother;

import pl.sda.barista.MilkIngredient;

public class MilkFrother {
    public FoamedMilkIngredient froth(MilkIngredient mleko) throws MilkFrotherException {
        if(Math.random()> 0.8) throw new MilkFrotherException("brak mleka");
        return new FoamedMilkIngredient();
    }
}
