package pl.sda.barista.pl.sda.frother;

import pl.sda.barista.CoffeeIngredient;

public class FoamedMilkIngredient extends CoffeeIngredient{
    public FoamedMilkIngredient() {
    }

    @Override
    public String toString() {
        return "FoamedMilkIngredient";
    }
}
