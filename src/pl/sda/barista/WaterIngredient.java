package pl.sda.barista;

public class WaterIngredient extends CoffeeIngredient{
    public int water;

    public WaterIngredient(int water) {
        this.water = water;
    }

    @Override
    public String toString() {
        return "Water";
    }
}
