package pl.sda.barista;

public class MilkIngredient extends CoffeeIngredient {
    @Override
    public String toString() {
        return "Milk";
    }
}
