package pl.sda.barista;

import java.util.List;

public class CoffeeBeverage {
    List<CoffeeIngredient> skladniki;

    public CoffeeBeverage(List<CoffeeIngredient> skladniki) {
        this.skladniki = skladniki;
    }

    @Override
    public String toString() {
        return "CoffeeBeverage{" +
                "skladniki=" + skladniki +
                '}';
    }
}
