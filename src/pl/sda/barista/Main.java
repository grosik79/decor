package pl.sda.barista;

import pl.sda.barista.pl.sda.frother.MilkFrother;
import pl.sda.barista.pl.sda.frother.MilkFrotherException;

public class Main {


    public static void main(String[] args) throws CoffieGrinderException, MilkFrotherException {
    AdvancedCoffeMachine  advancedCoffeMachine = new AdvancedCoffeMachine();
    advancedCoffeMachine.addIngredient(advancedCoffeMachine.grinder.grind(new CoffieBean()));
    advancedCoffeMachine.addIngredient(new WaterIngredient(30));
    advancedCoffeMachine.makeCoffeeBeverage();
        System.out.println( " Ekspresso =" + advancedCoffeMachine.makeCoffeeBeverage());

        advancedCoffeMachine.addIngredient(advancedCoffeMachine.grinder.grind(new CoffieBean()));
        advancedCoffeMachine.addIngredient(new WaterIngredient(100));
        advancedCoffeMachine.addIngredient(advancedCoffeMachine.frother.froth(new MilkIngredient()));
        advancedCoffeMachine.makeCoffeeBeverage();
        System.out.println("Latte = "+ advancedCoffeMachine.makeCoffeeBeverage());

        advancedCoffeMachine.addIngredient(advancedCoffeMachine.grinder.grind(new CoffieBean()));
        advancedCoffeMachine.addIngredient(new WaterIngredient(30));
        advancedCoffeMachine.addIngredient(advancedCoffeMachine.frother.froth(new MilkIngredient()));
        advancedCoffeMachine.makeCoffeeBeverage();

        System.out.println("Cappucino =" + advancedCoffeMachine.makeCoffeeBeverage());

    }

}
