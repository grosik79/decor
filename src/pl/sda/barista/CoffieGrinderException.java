package pl.sda.barista;

public class CoffieGrinderException extends Exception {
    public CoffieGrinderException(String message) {
        super(message);
    }
}
