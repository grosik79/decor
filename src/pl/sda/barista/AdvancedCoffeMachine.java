package pl.sda.barista;

import pl.sda.barista.pl.sda.frother.MilkFrother;
import pl.sda.barista.pl.sda.grinder.CoffeGrinder;

import java.util.ArrayList;
import java.util.List;

public class AdvancedCoffeMachine {
List<CoffeeIngredient> ingredients = new ArrayList<>();
MilkFrother frother = new MilkFrother();
CoffeGrinder grinder = new CoffeGrinder();


    public void addIngredient(CoffeeIngredient ingredient){
ingredients.add(ingredient);
    }
    public CoffeeBeverage makeCoffeeBeverage(){
       return new CoffeeBeverage(ingredients);
    }

}
